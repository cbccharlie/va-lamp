![logo.png](https://bitbucket.org/repo/RbAgok/images/2807292572-logo.png)

# VA#LAMP #

Entorno LAMP para Windows empleando Vagrant y Ansible. Está preparado para funcionar con las siguientes distribuciones:

* Debian Jessie
* Debian Wheezy
* Centos 7
* Centos 6



## Características principales ##

### Debian Jessie ###
* Apache 2.4
* PHP 5.6
* Varnish 4.0 (desactivado por defecto)
* MySQL 5.5
* OPCache
* Memcached
* Composer
* Drush 8

### Debian Wheezy ###
* Apache 2.2
* PHP 5.4
* Varnish 3.0 (desactivado por defecto)
* MySQL 5.5
* APC
* Memcached
* Composer
* Drush 8

### Centos 7 ###
* Apache 2.4
* PHP 5.4
* Varnish 4.0 (desactivado por defecto)
* MariaDB 5.5
* APC
* Memcached
* Composer
* Drush 8

### Centos 6 ###
* Apache 2.2
* PHP 5.3
* Varnish 2
* MySQL 5.1
* APC
* Memcached
* Composer
* Drush 6

### Herramientas ###
* Panel de control
* PHPINFO
* Adminer
* Backups de BBDD automáticos y manuales
* Panel de OPCache y APC
* Panel de Memcached
* Munin
* Mailhog
* Pimpmylog
* XDEBUG
* Xhprof



## Requisitos ##

* Activar **Virtualization Technology** en la BIOS
* Instalar [VirtualBox 5.1.12+](http://download.virtualbox.org/virtualbox/5.1.12/VirtualBox-5.1.12-112440-Win.exe)
* Instalar [Vagrant 1.8.7+](https://releases.hashicorp.com/vagrant/1.9.1/vagrant_1.9.1.msi) (no instalar la versión 1.9.1 debido a un bug con las interfaces de red)
* * Si se está actualizando, es posible que al terminar haya que ejecutar el siguiente comando **vagrant plugin repair** si al hacer un **vagrant up** da error.
* Disponer de una Shell (se recomienda usar una Shell con cliente SSH, como cygwin)



## Instalación ##

### Descargar los ficheros ###
Descargar todo el contenido del repositorio en una carpeta local. Esa carpeta debería contener los dos siguientes directorios:

* data
* machine

## Configuración inicial ##
Editar el fichero **"machine/config.yml"** para ajustar las siguientes propiedades:

* **human_name [requerido y único]:** Nombre que se mostrará en el VirtualBox.
* **hostname [requerido]:** Hostname de la máquina.
* **dist [requerido]:** La distribución elegida. Los valores posibles son **"debian"** o **"centos"**.
* **box [requerido]:** Box de Vagrant elegida. Los valores posibles son **"debian/jessie64"** o **"debian/wheezy64"** o **"centos/7"**.
* **box_url:** URL de la box a instalar (sólo se usará si se desea usar otra diferente a las indicadas en la propiedad anterior).
* **box_check_update:** Para activar o desactivar la comprobación de actualizaciones de la box. Los valores posibles son **true** o **false**.
* **private_network_enable:** Permite habilitar una interfaz de red privada. Los valores posibles son **true** o **false**.
* **private_network_dhcp:** Si se habilita esta opción, además de una interfaz de red privada, se asignará una IP dinámica. Los valores posibles son **true** o **false**.
* **private_network_ip:** Permite asignar una IP fija a la interfaz de red privada. Para usar esta opción tiene que dejarse a false la opción DHCP. **Se recomienda usar esta opción** e ir asignando diferentes IPs a las diferentes máquinas que se vayan creando. Por ejemplo, 172.28.128.10, 172.28.128.11, 172.28.128.12…
* **public_network_enable:** Permite habilitar una interfaz de red pública. Los valores posibles son **true** o **false**.
* **public_network_dhcp:** Si se habilita esta opción, además de una interfaz de red pública, se asignará una IP dinámica. Los valores posibles son **true** o **false**.
* **public_network_ip:** Permite asignar una IP fija a la interfaz de red pública. Para usar esta opción tiene que dejarse a false la opción DHCP. Por ejemplo, 192.168.1.10, 192.168.1.11… Solo usar esta opción con IPs libres para evitar conflictos de red en la intranet.
* **share_folders:** Permite compartir directorios entre la máquina virtual y Windows. Inicialmente viene configurado para compartir el directorio web, pero se pueden añadir tantos directorios como haga falta. Las opciones de configuración son:
* * **local_path:** Directorio en Windows.
* * **remote_path:** Directorio en la máquina virtual.
* * **type_sync:** Tipo de sincronización empleado. Los valores posibles son **nfs**, **virtualbox** y **rsync**. Se recomienda usar **nfs**. **Importante: para usar *nfs* es necesario que la máquina tenga una IP asignada, indiferentemente privada o pública, y fija o por DHCP.**
* **cpus:** Cantidad de cores de CPU asignadas a la máquina virtual.
* **memory:** Memoria RAM asignada a la máquina virtual.
* **gui:** Permite abrir la ventana de VirtualBox al arrancar la máquina. Los valores posibles son **true** o **false**.

## Otras configuraciones ##
Si se necesita modificar algunos de los valores predefinidos del entorno, por ejemplo, cambiar el memory_limit de PHP, crear un virtualhost nuevo en apache,… se modificará el fichero **"machine/ansible/vars/all.yml"**. En este fichero se encuentran todos los valores configurables con su valor estándar. Solo será necesario descomentar la línea y modificar su valor.

### Definir nuevos vhosts ###
Si se desean usar vhosts, se pueden definir tantos como se deseen en el fichero de configuración.

* Configurar las variables **apache_vhosts** y **apache_vhosts_ssl**, dependiendo del tipo de vhost que se quiera crear. Se puede usar como ejemplo el vhost por defecto, el cual **se debe mantener en caso de añadir alguno nuevo**.
* Para que el equipo local reconozca los nombres de dominio deben añadirse al fichero **hosts** de Windows. Abrir el bloc de notas en modo administrador:
* * Añadir una línea por cada host apuntando a la IP que se le asignara a la máquina virtual.

## Instalar plugins de Vagrant ##
Desde una Shell, instalar los siguientes plugins de vagrant (esto se hará la primera vez que se instale vagrant y no de cada vez que se arranque un nuevo entorno):

### vagrant-vbguest ###
```
#!bash

vagrant plugin install vagrant-vbguest
```
Esto instalará el plugin que actualiza automáticamente las guest additions tools. En caso de producirse un error en la instalación, deberemos descargar el plugin manualmente de la siguiente URL (https://rubygems.org/downloads/vagrant-vbguest-0.13.0.gem)[https://rubygems.org/downloads/vagrant-vbguest-0.13.0.gem] e instalarlo con el siguiente comando:
```
#!bash

vagrant plugin install RUTA-DEL-GEM-EN-EL-EQUIPO
```

### vagrant-winnfsd ###
```
#!bash

vagrant plugin install vagrant-winnfsd
```
Esto instalará el plugin que permite sincronizar carpetas por NFS. En caso de producirse un error en la instalación, deberemos descargar el plugin manualmente de la siguiente URL (https://rubygems.org/downloads/vagrant-winnfsd-1.3.1.gem)[https://rubygems.org/downloads/vagrant-winnfsd-1.3.1.gem] e instalarlo con el siguiente comando:
```
#!bash

vagrant plugin install RUTA-DEL-GEM-EN-EL-EQUIPO
```



## Desplegar el entorno ##
Acceder a machine, dentro del directorio de antes y ejecutar el siguiente comando para arrancar e inicializar el entorno (la primera vez le llevará unos minutos, ya que instalará y actualizará toda la paquetería necesaria):

```
#!bash

vagrant up
```

## Comandos básicos de Vagrant ##

```
#!bash

vagrant up
```
Arrancar el entorno.

```
#!bash

vagrant halt
```
Parar el entorno (es importante apagar el entorno con este comando para evitar que pueda corromperse el entorno).

```
#!bash

vagrant destroy
```
Eliminar el entorno (si se usa una versión antigua de cygwin será necesario ajustar una variable de entorno antes “export VAGRANT_DETECTED_OS=Cygwin”).

```
#!bash

vagrant provision
```
Volver a ejecutar el fichero de instalación sin destruir la máquina (usado en actualizaciones).

```
#!bash

vagrant ssh
```
Conectarse a la máquina por SSH (necesario tener un cliente con openssh. Si queremos conectarnos a la máquina con putty o similar, los datos de acceso son vagrant:vagrant).



## Posible problemas y errores ##
* Será necesario tener activado en la bios **Virtualization Technology**. Algunos equipos vienen con ello desactivado. Normalmente detectaremos este error porque la consola se quedará mostrando timeouts continuamente sin llegar a conectar nunca.
* Error al hacer el **vagrant up**: Probar a ejecutar la consola como administrador.
* Si se produce el error de abajo, será necesario ejecutar los siguientes comandos:
* * **VBoxManage list dhcpservers**: Esto listará todo los servidores dhcp y, salvo que tengamos máquinas distintas a las de vagrant, los eliminaremos todos.
* * **VBoxManage dhcpserver remove --netname [NOMBREDCHPSERVER]**: Con este comando eliminaremos cada uno de los servidores dhcp listados reemplazando [NOMBREDCHPSERVER] por cada uno de los nombres.
**A HOST ONLY NETWORK INTERFACE YOU'RE ATTEMPTING TO CONFIGURE VIA DHCP ALREADY HAS A CONFLICTING HOST ONLY ADAPTER WITH DHCP ENABLED. THE DHCP ON THIS ADAPTER IS INCOMPATIBLE WITH THE DHCP SETTINGS. TWO HOST ONLY NETWORK INTERFACES ARE NOT ALLOWED TO OVERLAP, AND EACH HOST ONLY NETWORK INTERFACE CAN HAVE ONLY ONE DHCP SERVER. PLEASE RECONFIGURE YOUR HOST ONLY NETWORK OR REMOVE THE VIRTUAL MACHINE USING THE OTHER HOST ONLY NETWORK.**
* Errores de Ansible:
* * /usr/local/lib/python2.7/dist-packages/cryptography/hazmat/backends/openssl/backend.py:307: UserWarning: implicit cast from 'char *' to a different pointer type: will be forbidden in the future (check that the types are as you expect; use an explicit ffi.cast() if they are correct)
* * * **Solución**: Actualizar cryptography a 1.5.1+



## Capturas ##

![captura.png](https://bitbucket.org/repo/RbAgok/images/2761972240-captura.png)