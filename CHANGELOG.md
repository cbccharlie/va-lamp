# CHANGELOG #

## 1.2.0 ##

* Error de dependencia de ansible (jinja2).
* Cambiar vhost por defecto para que al añadir otros siga siendo el por defecto.
* Cambiar plantilla de los virtualhost para que liste los archivos al acceder a un directorio.
* Puerto por defecto de Varnish ahora es el 8080. En caso de que se asigne el 80 y se active, el puerto de apache pasa a ser el 8080.
* Error al sobreescribir algunas variables: Se modifica la estructura para que algunas ya no estén en el fichero all.yml (no tiene sentido que estén) y se soluciona las que no se podían sobreescribir.
* xhprof: Activado por defecto y solucionado error al crear el directorio temporal.
* Ahora innodb_file_per_table está activado por defecto y es configurable.

## 1.3.0 ##

* Añadir Centos 6 como distribución soportada.
* Corregir permisos de los logs cuando se realiza el rotado.
* Añadir mensaje de bienvenida al entrar por SSH.
* Añadida variable en los vhost para definir la carpeta raíz en la que estará el proyecto en vez de usar el document_root, en el cual podríamos querer tener otro valor.

## 1.3.1 ##

* Añadir al fichero de variables general los paquetes de PHP de Centos 6.
* Añadir el paquete ssl-cert en Debian 7 para tener disponibles los certificados de desarrollo.
* Corregir problema con vhosts en SSL en Debian 7.
* Añadir el server name de los vhosts en el archivo hosts de la máquina.
* Comprobar si existe el fichero de configuración de xdebug antes de modificarlo, para evitar errores en caso de que no se instale el paquete.
* Asegurar que existe el directorio donde se colocan los scripts de backups de BBDD.
* La variable ServerAlias de los vhosts ahora es un array, para poder crear varios.