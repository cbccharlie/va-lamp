---
#
# INCLUDE OS VARS.
#
- name: Include OS variables.
  include_vars: "{{ ansible_distribution|lower }}.yml"

- name: Include OS version variables.
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"

#
# INSTALL PHP AND PACKAGES.
#
- name: Install PHP.
  package:
    pkg: "{{ php_package }}"
    state: latest

- name: Install PHP packages.
  package:
    pkg: "{{ item }}"
    state: latest
  with_items: "{{ php_packages_override|default(php_packages) }}"
  when: php_packages is defined
  notify: restart apache

#
# CREATE VARIABLE WITH PHP INSTALLED VERSION.
#
- name: Get installed version of PHP.
  shell: "{{ php_package }} -v"
  changed_when: false
  check_mode: no
  register: _php_version

- name: Create php version variable.
  set_fact:
    _php_version: "{{ _php_version.stdout.split()[1].split('.')[0] }}.{{ _php_version.stdout.split()[1].split('.')[1] }}"

#
# CONFIGURE PHP.INI.
#
- name: Configure php.ini.
  lineinfile:
    dest: "{{ php_ini_file }}"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    state: present
  with_items: "{{ php_ini_items }}"
  notify: restart apache

#
# CONFIGURE OPCACHE.
#
- name: Configure OPCache.
  lineinfile:
    dest: "{{ php_ini_file }}"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    state: present
  with_items: "{{ php_ini_opcache_items }}"
  notify: restart apache
  when: _php_version|float >= 5.6

#
# INSTALL APC.
#
- name: Check if apc file exist.
  stat:
    path: "{{ php_apc_file }}"
  register: p
  when: _php_version|float < 5.6

- name: Install APC.
  pear:
    name: pecl/apc
    state: present
  when: _php_version|float < 5.6 and p.stat.exists == false

- name: Configure APC.
  template:
    src: "apc.ini.j2"
    dest: "{{ php_apc_file }}"
    owner: root
    group: root
    mode: 0644
  notify: restart apache
  when: _php_version|float < 5.6

- name: Enable APC extension.
  command: php5enmod apc
  notify: restart apache
  when: _php_version|float < 5.6 and ansible_distribution == "Debian" and p.stat.exists == false

#
# CONFIGURE XDEBUG.
#
- name: Check if xdebug file exist.
  stat:
    path: "{{ php_xdebug_file }}"
  register: p

- name: Configure Xdebug.
  lineinfile:
    dest: "{{ php_xdebug_file }}"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    state: present
  with_items: "{{ php_xdebug_items }}"
  notify: restart apache
  when: p.stat.exists == true

#
# CREATE ERROR_LOG FILE.
#
- name: Check if php_errors.log exists.
  stat:
    path: "/var/log/{{ apache_package }}/php_error.log"
  register: phperrorlog

- name: Create error_log file.
  file:
    path: "/var/log/{{ apache_package }}/php_error.log"
    state: touch
    owner: "{{ apache_owner }}"
    group: "{{ apache_group }}"
    mode: 0644
  when: phperrorlog.stat.exists == false