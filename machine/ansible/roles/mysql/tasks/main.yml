---
#
# INCLUDE OS VARS.
#
- name: Include OS variables.
  include_vars: "{{ ansible_distribution|lower }}.yml"

- name: Include OS version variables.
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"

#
# INSTALL MYSQL.
#
- name: Install {{ mysql_package }}.
  package:
    pkg: "{{ mysql_package }}"
    state: latest

#
# INSTALL PYTHON MYSQL LIBRARY.
#
- name: Install {{ mysql_python_package }}.
  package:
    pkg: "{{ mysql_python_package }}"
    state: latest

#
# CHECK SERVICE STATUS.
#
- name: Ensure {{ mysql_package }} has selected state and enabled on boot.
  service:
    name: "{{ mysql_service }}"
    state: "{{ mysql_state }}"
    enabled: yes

#
# INCLUDE CONF FILES.
#
- name: Ensure that mysql conf directory exists.
  file:
    path: "{{ mysql_config_dir }}"
    state: directory
    mode: 0755

- name: Include conf files.
  lineinfile:
    dest: "{{ mysql_config_file }}"
    line: "!includedir {{ mysql_config_dir }}"
    state: present
  notify: restart mysql

#
# CONFIGURE MY.CNF CUSTOM FILE.
#
- name: Configure my.cnf custom file.
  template:
    src: "custom.cnf.j2"
    dest: "{{ mysql_config_dir }}{{ mysql_config_file_custom }}"
    mode: 0644
  notify: restart mysql

#
# REMOVE ALL ANONYMOUS USER ACCOUNTS.
#
- name: Remove all anonymous user accounts.
  mysql_user:
    name: ''
    host_all: yes
    state: absent

#
# GRANT EXTERNAL ACCESS TO ROOT USER.
#
- name: Grant external access to root user.
  mysql_user:
    name: "root"
    password: ""
    host: "%"
    priv: "*.*:ALL,GRANT"
    state: present
  notify: restart mysql
  when: mysql_external_access_root == true

#
# CREATE ANOTHER USERS.
#
- name: Create users.
  mysql_user:
    name: "{{item.name}}"
    password: "{{item.pass}}"
    host: "{{item.host}}"
    priv: "{{item.priv}}"
    state: present
  with_items: "{{ mysql_users|default([]) }}"
  notify: restart mysql

#
# CONFIGURE SCRIPT FOR AUTOMATIC BACKUPS.
#
- name: Create directory for backup scripts.
  file:
    path: "{{ mysql_backups_script_path }}"
    state: directory
    mode: 0755

- name: MYSQL create script for administer backups.
  template:
    src: "mysql_backups.sh.j2"
    dest: "{{ mysql_backups_script_path }}{{ mysql_backups_backup_script_filename }}"
    mode: 0755
  when: mysql_backups_enable == true

#
# CONFIGURE SCRIPT FOR RESTORE BACKUPS.
#
- name: MYSQL create script for restore backups.
  template:
    src: "mysql_restore.sh.j2"
    dest: "{{ mysql_backups_script_path }}{{ mysql_backups_restore_script_filename }}"
    mode: 0777
  when: mysql_backups_enable == true

#
# CONFIGURE CRON TASK.
#
- name: MYSQL cron task for create/clean backups.
  cron:
    name: "mysql backups"
    minute: "{{ mysql_backups_cron_minute }}"
    hour: "{{ mysql_backups_cron_hour }}"
    user: "root"
    job: "{{ mysql_backups_script_path }}{{ mysql_backups_backup_script_filename }} > /dev/null"
    cron_file: mysql_backups
  when: mysql_backups_enable == true

- name: Add environment variable SHELL into cron file.
  cron:
    name: SHELL
    env: yes
    value: /bin/bash
    user: "root"
    cron_file: mysql_backups
  when: mysql_backups_enable == true

- name: Add environment variable PATH into cron file.
  cron:
    name: PATH
    env: yes
    value: /bin:/sbin:/usr/bin:/usr/sbin
    user: "root"
    cron_file: mysql_backups
  when: mysql_backups_enable == true