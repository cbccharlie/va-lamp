---
#
# INCLUDE OS VARS.
#
- name: Include OS variables.
  include_vars: "{{ ansible_distribution|lower }}.yml"

- name: Include OS version variables.
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"

#
# INSTALL PACKAGES.
#
- name: Install common packages.
  package:
    pkg: "{{ item }}"
    state: latest
  with_items: "{{ munin_common_packages }}"

- name: Install specific packages.
  package:
    pkg: "{{ item }}"
    state: latest
  with_items: "{{ munin_specific_packages }}"

#
# MUNIN APACHE CONF.
#
- name: Munin Debian apache conf.
  template:
    src: "apache.conf.j2"
    dest: "{{ munin_apache_conf_file }}"
  notify: restart apache
  when: (ansible_distribution == 'Debian')

#
# MUNIN APACHE CONF - REMOVE PROTECTION.
#
- name: Munin Centos apache conf - Remove protection.
  lineinfile:
    dest: "{{ munin_apache_conf_file }}"
    regexp: "{{ item.regexp }}"
    state: absent
  with_items:
    - { regexp: '^AuthUserFile /etc/munin/munin-htpasswd' }
    - { regexp: '^AuthName "Munin"' }
    - { regexp: '^AuthType Basic' }
    - { regexp: '^require valid-user' }
  notify: restart apache
  when: (ansible_distribution == 'CentOS')

- name: Munin Centos apache conf - Set alias.
  lineinfile:
    dest: "{{ munin_apache_conf_file }}"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    insertbefore: "{{ item.insertbefore }}"
  with_items:
    - { regexp: "^Alias /stats/munin /var/cache/munin/www", line: "Alias /stats/munin /var/cache/munin/www", insertbefore: "^<directory" }
    - { regexp: "^<directory ", line: "<directory /var/cache/munin/www>", insertbefore: "^</directory>" }
    - { regexp: "^Require all", line: "Require all granted", insertbefore: "^</directory>" }
    - { regexp: "^Options ", line: "Options FollowSymLinks SymLinksIfOwnerMatch", insertbefore: "^</directory>" }
  notify: restart apache
  when: (ansible_distribution == 'CentOS')

#
# ENSURE THAT CONFIG DIRECTORY EXISTS.
#
- name: Ensure that config directory exists.
  file:
    path: "/var/cache/munin/www"
    state: directory
    owner: munin
    group: munin
    mode: 0755

#
# CONFIG WWW DIRECTORY.
#
- name: Config www directory.
  lineinfile:
    dest: "/etc/munin/munin.conf"
    line: "htmldir /var/cache/munin/www"
    regexp: "^(|#)htmldir"
  notify: restart munin

#
# ENABLE APACHE/MYSQL PLUGINS.
#
- name: Enable apache/mysql plugins.
  file:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    state: link
  with_items:
    - { src: '/usr/share/munin/plugins/apache_accesses', dest: '/etc/munin/plugins/apache_accesses' }
    - { src: '/usr/share/munin/plugins/apache_processes', dest: '/etc/munin/plugins/apache_processes' }
    - { src: '/usr/share/munin/plugins/apache_volume', dest: '/etc/munin/plugins/apache_volume' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_bin_relay_log' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_commands' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_connections' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_files_tables' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_bpool' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_bpool_act' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_insert_buf' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_io' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_io_pend' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_log' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_rows' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_semaphores' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_innodb_tnx' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_myisam_indexes' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_network_traffic' }
    - { src: '/usr/share/munin/plugins/mysql_', dest: '/etc/munin/plugins/mysql_qcache' }
  notify: restart munin

#
# REMOVE INICIAL WWW DIRECTORY.
#
- name: Remove inicial www directory.
  file:
    state: absent
    path: "/var/www/html/munin/"

#
# START MUNIN ON BOOT.
#
- name: Start munin on boot.
  service:
    name: "munin-node"
    enabled: yes