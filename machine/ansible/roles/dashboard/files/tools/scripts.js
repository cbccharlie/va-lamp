$(function() {

  $('.launch_backups').click(function(e){
    e.preventDefault();
    show_loading();
    $.ajax({
      url: 'index.php',
      data: { action : 'launch_backups' },
      type: 'POST',
      success : function(json) {
        alert('Backups realizados correctamente.');
      },
      error : function(xhr, status) {
        alert('Disculpe, existió un problema.');
      },
      complete : function(xhr, status) {
        $('.throbber_wrapper').remove();
        $('.throbber').remove();
      }
    });
    return false;
  });

  $('.restore_backup').click(function(e){
    var url = $(this).attr('href');
    var file = getURLParameter(url, 'file');
    var db = getURLParameter(url, 'db');
    e.preventDefault();
    show_loading();
    $.ajax({
      url: 'index.php',
      data: { action : 'restore_backup', 'file': file, 'db': db },
      type: 'POST',
      success : function(json) {
        alert('BBDD restaurada correctamente.');
      },
      error : function(xhr, status) {
        alert('Disculpe, existió un problema.');
      },
      complete : function(xhr, status) {
        $('.throbber_wrapper').remove();
        $('.throbber').remove();
      }
    });
    return false;
  });

  $(".backups h3 a").click(function () {
    $header = $(this).parent();
    $content = $header.next();
    $content.slideToggle(500, function () {
      $header.attr('class', function () {
        return $content.is(":visible") ? "active" : "";
      });
    });
    return false;
  });

});
function show_loading() {
  $('body').append('<div class="throbber_wrapper">&nbsp;</div><div class="throbber"><i class="fa fa-refresh fa-spin fa-5x fa-fw"></i></div>');
}
function getURLParameter(url, name) {
  return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}