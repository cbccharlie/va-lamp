# Update and install dependencies
apt-get update && apt-get -y upgrade

if ! type "ansible" > /dev/null; then
  apt-get install -y python-dev libffi-dev libssl-dev build-essential wget sudo

  # Install pip
  wget https://bootstrap.pypa.io/get-pip.py --no-check-certificate
  sudo python get-pip.py

  # Install Ansible
  sudo pip install paramiko PyYAML Jinja2==2.8 httplib2 six ansible 'requests[security]'
fi

# Run ansible-playbook
export ANSIBLE_HOST_KEY_CHECKING=False
sudo ansible-playbook -i localhost, /vagrant/ansible/playbook.yml --connection=local